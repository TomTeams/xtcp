#ifndef XTCPSERVER_H
#define XTCPSERVER_H
#include "xtcpserver_global.h"
#include <QHostAddress>
#include <QTcpServer>
#include <QTcpSocket>
#include <QUuid>
class XTcpSocket;
class ConnectionKey
{
public:
	inline bool operator==( const ConnectionKey& key ) const
	{
		return ( key.address() == _address && key.id() == _id );
	}
	inline QHostAddress address() const
	{
		return _address;
	}
	inline QUuid id() const
	{
		return _id;
	}
	inline ConnectionKey( QHostAddress address, QUuid id )
	{
		_address = address;
		_id      = id;
	}

private:
	QHostAddress _address;
	QUuid _id;
};
inline uint qHash( ConnectionKey key )
{
	QString str = QString( "%0%1" ).arg( key.address().toString() ).arg( key.id().toString() );
	return qHash( str );
}
class XTCPSERVERSHARED_EXPORT XTcpServer : public QTcpServer
{
	Q_OBJECT

public:
	XTcpServer( QObject* parent = nullptr );
	void closeConnections();


public slots:
	void send( const ConnectionKey& address, const QByteArray& data );

signals:
	void dataObtain( const ConnectionKey& key, const QByteArray& data );
	void error( QTcpSocket::SocketError error );
	/**
	 * @brief connectionCountChanged Signal is emited whenever connection cout was changed
	 * @param count
	 */
	void connectionCountChanged( int count );

	// QTcpServer interface
protected:
	/**
	 * @brief incomingConnection This override function is colled when new connection is available. It create new XTcpServerThread object
	 * witch passed to him socket descriptor. XTcpServerThread create new QTcpSocket with will be added to pendding connection.
	 * @param handle
	 */
	void incomingConnection( qintptr handle ) override;

protected slots:
	void disconnected();
	void onDataObtain( const QByteArray& data );

private:
	QHash< ConnectionKey, XTcpSocket* > _connections;
};

#endif // XTCPSERVER_H
