#include "XTcpServer.h"
#include "../../XTcpSocket/include/XTcpSocket.h"
#include <QTcpSocket>
XTcpServer::XTcpServer( QObject* parent )
	: QTcpServer( parent )
{
	setMaxPendingConnections( 99 );
}

void XTcpServer::closeConnections()
{
	for ( auto connection = _connections.cbegin(); connection != _connections.cend(); ++connection )
	{
		connection.value()->disconnectFromHost();
	}
}

void XTcpServer::send( const ConnectionKey& address, const QByteArray& data )
{
	if ( auto socket = _connections.value( address ) )
	{
		socket->sendData( data );
	}
}

void XTcpServer::incomingConnection( qintptr handle )
{
	auto socket = new XTcpSocket( handle );
	socket->start();
	auto attemps = 0;
	while ( !socket->started() && attemps > 10 )
	{
		thread()->wait( 10 );
		++attemps;
	}
	connect( socket, &XTcpSocket::dataObtain, this, &XTcpServer::onDataObtain, Qt::QueuedConnection );
	connect( socket, &XTcpSocket::error, this, &XTcpServer::error, Qt::QueuedConnection );
	connect( socket, &XTcpSocket::disconnected, this, &XTcpServer::disconnected, Qt::QueuedConnection );
	addPendingConnection( socket->socket() );
	_connections.insert( ConnectionKey( socket->peerAddress(), QUuid::createUuid() ), socket );
	emit connectionCountChanged( _connections.count() );
}

void XTcpServer::disconnected()
{
	if ( auto socket = dynamic_cast< XTcpSocket* >( sender() ) )
	{
		auto key = _connections.key( socket, ConnectionKey( QHostAddress(), QUuid() ) );
		_connections.remove( key );
		emit connectionCountChanged( _connections.count() );
		socket->deleteLater();
	}
}

void XTcpServer::onDataObtain( const QByteArray& data )
{
	if ( auto socket = dynamic_cast< XTcpSocket* >( sender() ) )
	{
		auto key = _connections.key( socket, ConnectionKey( QHostAddress(), QUuid() ) );
		emit dataObtain( key, data );
	}
}
