-------------------------------------------------
#
# Project created by QtCreator 2018-12-25T21:24:49
#
#-------------------------------------------------
CONFIG += c++11
QT       += network xml
QT       -= gui
QMAKE_CXXFLAGS += -fopenmp
QMAKE_CXXFLAGS += -static-libstdc++
QMAKE_CFLAGS += -static-libstdc++
QMAKE_LFLAGS  += -static-libstdc++

TARGET = XTcpServer
TEMPLATE = lib
DESTDIR = $$PWD/../../Deploy/

DEFINES += XTCPSERVER_LIBRARY

SOURCES += \
        src/XTcpServer.cpp

HEADERS += \
        include/XTcpServer.h \
        include/xtcpserver_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
LIBS += -L$$PWD/../../Deploy/ -lXTcpSocket

INCLUDEPATH += $$PWD/include
INCLUDEPATH += $$PWD/../../Deploy
DEPENDPATH += $$PWD/../../Deploy
