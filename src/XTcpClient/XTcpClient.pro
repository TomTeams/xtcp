#-------------------------------------------------
#
# Project created by QtCreator 2018-12-31T07:08:48
#
#-------------------------------------------------
CONFIG += c++11
QT       += network xml
QT       -= gui

QMAKE_CXXFLAGS += -fopenmp
QMAKE_CXXFLAGS += -static-libstdc++
QMAKE_CFLAGS += -static-libstdc++
QMAKE_LFLAGS  += -static-libstdc++


TARGET = XTcpClient
TEMPLATE = lib
DESTDIR = $$PWD/../../Deploy/

DEFINES += XTCPCLIENT_LIBRARY

SOURCES += \
        src/XTcpClient.cpp

HEADERS += \
        include/XTcpClient.h \
        include/xtcpclient_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

LIBS += -L$$PWD/../../Deploy/ -lXTcpSocket

INCLUDEPATH += $$PWD/include
INCLUDEPATH += $$PWD/../../Deploy
DEPENDPATH += $$PWD/../../Deploy
