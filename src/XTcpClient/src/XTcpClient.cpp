#include <utility>

#include "../../XTcpSocket/include/XTcpSocket.h"
#include "XTcpClient.h"


XTcpClient::XTcpClient( const QString& host, quint16 port )
	: _host( host )
	, _port( port )
	, _eof( char( 04 ) )
	, _automaticReconnect( true )
	, _reconnectInterval( 1000 )
	, _state( QAbstractSocket::UnconnectedState )
	, _socket( new XTcpSocket( _host, _port, _eof, _automaticReconnect, _reconnectInterval ) )
{
	connect( _socket, &XTcpSocket::error, this, &XTcpClient::error, Qt::QueuedConnection );
	connect( _socket, &XTcpSocket::dataObtain, this, &XTcpClient::parseData, Qt::QueuedConnection );
	connect( _socket, &XTcpSocket::connectionStateChanged, this, &XTcpClient::setConnectionState, Qt::QueuedConnection );
}

XTcpClient::~XTcpClient()
{
	delete _socket;
}

void XTcpClient::sendData( QByteArray msg )
{
	_socket->sendData( msg );
}

void XTcpClient::sendData( const QString& msg )
{
	QByteArray array;
	array.append( msg );
	sendData( array );
}

void XTcpClient::connectToServer()
{
	_socket->start();
}

void XTcpClient::disconnectFromServer()
{
	_socket->disconnectFromHost();
	_socket->quit();
}

QAbstractSocket::SocketState XTcpClient::connectionState()
{
	return _state;
}

char XTcpClient::eof() const
{
	return _eof;
}

void XTcpClient::setEof( char eof )
{
	_eof = eof;
}

quint16 XTcpClient::reconnectInterval() const
{
	return _reconnectInterval;
}

void XTcpClient::setReconnectInterval( const quint16& reconnectInterval )
{
	_reconnectInterval = reconnectInterval;
}

bool XTcpClient::automaticReconnect() const
{
	return _automaticReconnect;
}

void XTcpClient::setAutomaticReconnect( bool automaticReconnect )
{
	_automaticReconnect = automaticReconnect;
}

void XTcpClient::setConnectionState( QAbstractSocket::SocketState state )
{
	if ( _state != state )
	{
		_state = state;
		emit connectionStateChanged( _state );
	}
}

void XTcpClient::parseData( QByteArray msg )
{
	emit dataObtain( msg );
}
