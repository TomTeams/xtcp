#ifndef XTCPCLIENT_H
#define XTCPCLIENT_H
#include "xtcpclient_global.h"
#include <QTcpSocket>
class XTcpSocket;
class XTCPCLIENTSHARED_EXPORT XTcpClient : public QObject
{
	Q_OBJECT
public:
	XTcpClient( const QString& host, quint16 port );
	~XTcpClient() override;
	void sendData( QByteArray msg );
	void sendData( const QString& msg );

	void connectToServer();
	void disconnectFromServer();

	QTcpSocket::SocketState connectionState();

	char eof() const;
	void setEof( char eof );

	bool automaticReconnect() const;
	void setAutomaticReconnect( bool automaticReconnect );

	quint16 reconnectInterval() const;
	void setReconnectInterval( const quint16& reconnectInterval );

signals:
	void error( QTcpSocket::SocketError error );
	void connectionStateChanged( QAbstractSocket::SocketState state );
	void dataObtain( QByteArray msg );

private:
	QString _host;
	quint16 _port;
	char _eof;
	bool _automaticReconnect;
	quint16 _reconnectInterval;
	QAbstractSocket::SocketState _state;
	XTcpSocket* _socket;

private slots:
	void setConnectionState( QAbstractSocket::SocketState state );
	void parseData( QByteArray msg );
};

#endif // XTCPCLIENT_H
