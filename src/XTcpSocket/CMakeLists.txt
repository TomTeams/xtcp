cmake_minimum_required( VERSION 3.0 )
project( XTcpSocket VERSION 1.0.0  LANGUAGES CXX )
#qt
find_package(Qt5 COMPONENTS Core Network REQUIRED)

if(${CMAKE_PROJECT_NAME} STREQUAL ${PROJECT_NAME} )
	set( CMAKE_CXX_STANDARD 11 )
	set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin )
	set( CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/lib )
	set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/lib )
#doxygen
find_package( Doxygen )
if( DOXYGEN_FOUND)
	set( DOXYGEN_PROJECT_NAME ${PROJECT_NAME} )
	set( DOXYGEN_PROJECT_VERSION ${CMAKE_PROJECT_VERSION} )
	set( DOXYGEN_GENERATE_HTML YES )
	set( DOXYGEN_GENERATE_QHP YES )
	set( DOXYGEN_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/doc/)
	set( DOXYGEN_QCH_FILE ${DOXYGEN_OUTPUT_DIRECTORY}/${PROJECT_NAME}.qch)
	set( DOXYGEN_QHP_NAMESPACE ${PROJECT_NAME} )
	set( DOXYGEN_QHG_LOCATION /usr/bin/qhelpgenerator-qt5)
	set( DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/build/CMakeDoxyfile.in)
	set( DOXYGEN_INPUT ${CMAKE_SOURCE_DIR}/include/)
	set( DOXYGEN_OUT ${CMAKE_SOURCE_DIR}/doc/Doxyfile )
	configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)
	add_custom_target( ${PROJECT_NAME}Doc ALL
		   COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
		   WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
		   COMMENT "Generating API documentation with Doxygen"
		   VERBATIM )
	doxygen_add_docs(doxygen ${PROJECT_SOURCE_DIR} COMMENT "documentation generated" )
else()
	message( "doxygen package not found" )
endif( DOXYGEN_FOUND )
endif()
include_directories(include/)

qt5_wrap_cpp(
    MOC_FILES
    "include/XTcpSocketWorker.h"
    "include/XTcpSocket.h" )

add_library( ${PROJECT_NAME} SHARED
        src/XTcpSocket.cpp
	src/XTcpSocketWorker.cpp
	${MOC_FILES})
target_link_libraries( ${PROJECT_NAME}
#	#qt
	PRIVATE
	Qt5::Core
	Qt5::Network)
#installation
if(UNIX)
	set_target_properties ( XTcpSocket PROPERTIES PUBLIC_HEADER "include/XTcpSocket.h;include/xtcpsocket_global.h" )
	install( TARGETS XTcpSocket  DESTINATION  lib/X PUBLIC_HEADER DESTINATION include/X  )
	install( FILES XTcpSocketConfig.cmake DESTINATION /usr/local/lib/cmake )
elseif ( WIN32 )
endif()
