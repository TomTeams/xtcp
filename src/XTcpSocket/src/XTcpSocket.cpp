#include "XTcpSocket.h"
#include "XTcpSocketWorker.h"
#include <QTimer>

XTcpSocket::XTcpSocket( QString host, quint16 port, char eof, bool automaticReconnect, quint16 reconnectInterval )
	: _host( std::move( host ) )
	, _port( port )
	, _eof( eof )
	, _automaticReconnect( automaticReconnect )
	, _reconnectInterval( reconnectInterval )
	, _state( QTcpSocket::UnconnectedState )
	, _socketDescriptor( -1 )
	, _started( false )
{
	qRegisterMetaType< QAbstractSocket::SocketState >( "QAbstractSocket::SocketState" );
}

XTcpSocket::XTcpSocket( qintptr socketDescriptor, char eof, bool automaticReconnect, quint16 reconnectInterval )
	: _eof( eof )
	, _automaticReconnect( automaticReconnect )
	, _reconnectInterval( reconnectInterval )
	, _state( QTcpSocket::UnconnectedState )
	, _socketDescriptor( socketDescriptor )
	, _started( false )
{
	qRegisterMetaType< QAbstractSocket::SocketState >( "QAbstractSocket::SocketState" );
}

XTcpSocket::~XTcpSocket()
{
	quit();
	wait( 1000 );
}

void XTcpSocket::sendData( QByteArray msg )
{
	emit pushData( std::move( msg ) );
}

void XTcpSocket::disconnectFromHost()
{
	emit dropHostConnection();
}

void XTcpSocket::setEof( char eof )
{
	_eof = eof;
}

QAbstractSocket::SocketState XTcpSocket::connectionState()
{
	return _state;
}

char XTcpSocket::eof() const
{
	return _eof;
}

QHostAddress XTcpSocket::peerAddress() const
{
	return _address;
}

quint16 XTcpSocket::peerPort() const
{
	return _socket->peerPort();
}


void XTcpSocket::run()
{
	XTcpSocketWorker* worker;
	if ( _socketDescriptor == -1 )
	{
		worker = new XTcpSocketWorker( _host, _port, _eof, _automaticReconnect, _reconnectInterval );
	}
	else
	{
		worker = new XTcpSocketWorker( _socketDescriptor, _eof, _automaticReconnect, _reconnectInterval );
	}

	connect( worker, &XTcpSocketWorker::error, this, &XTcpSocket::error, Qt::QueuedConnection );
	connect( worker, &XTcpSocketWorker::disconnected, this, &XTcpSocket::disconnected, Qt::QueuedConnection );
	connect( worker, &XTcpSocketWorker::connectionStateChanged, this, &XTcpSocket::setConnectionState, Qt::QueuedConnection );
	connect( worker, &XTcpSocketWorker::dataObtain, this, &XTcpSocket::dataObtain, Qt::QueuedConnection );
	connect( worker, &XTcpSocketWorker::peerAddressChanged, this, &XTcpSocket::setPeerAddress );
	connect( this, &XTcpSocket::pushData, worker, &XTcpSocketWorker::sendData, Qt::QueuedConnection );
	connect( this, &XTcpSocket::dropHostConnection, worker, &XTcpSocketWorker::deleteLater, Qt::QueuedConnection );
	connect( this, &XTcpSocket::destroyed, worker, &XTcpSocketWorker::deleteLater, Qt::QueuedConnection );
	setConnectionState( worker->connectionState() );
	_address = worker->socket()->peerAddress();
	_port    = worker->socket()->peerPort();
	setSocket( worker->socket() );
	_started = true;
	exec();
}

bool XTcpSocket::started() const
{
	return _started;
}

void XTcpSocket::setSocket( QTcpSocket* socket )
{
	_socket = socket;
}

QTcpSocket* XTcpSocket::socket() const
{
	return _socket;
}

void XTcpSocket::setConnectionState( QAbstractSocket::SocketState state )
{
	if ( _state != state )
	{
		_state = state;
		emit connectionStateChanged( _state );
	}
}

void XTcpSocket::setPeerAddress( QHostAddress address )
{
	_address = address;
}
