#include "XTcpSocketWorker.h"
#include <QTimer>

XTcpSocketWorker::XTcpSocketWorker( QString host, quint16 port, char eof, bool automaticReconnect, quint16 reconnectInterval )
    : _host( std::move( host ) )
    , _port( port )
    , _eof( eof )
    , _automaticReconnect( automaticReconnect )
    , _reconnectionTry( 0 )
    , _reconnectInterval( reconnectInterval )
    , _timer( new QTimer( this ) )
    , _pushDataTimer( new QTimer( this ) )
    , _socket( new QTcpSocket() )
{
	qRegisterMetaType< QHostAddress >( "QHostAddress" );
	_incomeDataBuffer.clear();
	setupConnection();
	_socket->connectToHost( _host, _port );
	_socket->waitForConnected();
	setupTimers();
}

XTcpSocketWorker::XTcpSocketWorker( qintptr socketDescriptor, char eof, bool automaticReconnect, quint16 reconnectInterval )
    : _eof( eof )
    , _automaticReconnect( automaticReconnect )
    , _reconnectionTry( 0 )
    , _reconnectInterval( reconnectInterval )
    , _timer( new QTimer( this ) )
    , _pushDataTimer( new QTimer( this ) )
    , _socket( new QTcpSocket() )
{
	qRegisterMetaType< QHostAddress >( "QHostAddress" );
	_incomeDataBuffer.clear();
	_socket->setSocketDescriptor( socketDescriptor );
	setupConnection();
	setupTimers();
}

XTcpSocketWorker::~XTcpSocketWorker()
{
	disconnectFromHost();
	delete _timer;
	delete _socket;
}

void XTcpSocketWorker::setupConnection()
{
	connect( _timer, &QTimer::timeout, this, &XTcpSocketWorker::checkConnectionState, Qt::QueuedConnection );
	connect( _pushDataTimer, &QTimer::timeout, this, &XTcpSocketWorker::pushData, Qt::QueuedConnection );
	connect( _socket, &QTcpSocket::readyRead, this, &XTcpSocketWorker::readData, Qt::QueuedConnection );
	connect( _socket, &QTcpSocket::disconnected, this, &XTcpSocketWorker::disconnected, Qt::QueuedConnection );
	connect( _socket, &QTcpSocket::stateChanged, this, &XTcpSocketWorker::connectionStateChanged, Qt::QueuedConnection );
	connect( _socket, &QTcpSocket::stateChanged, this, [this]( QAbstractSocket::SocketState state ) {
		emit connectionStateChanged( state );
		emit peerAddressChanged( _socket->peerAddress() );
	} );
}

void XTcpSocketWorker::setupTimers()
{
	_timer->setInterval( 300 );
	_timer->start();
	_pushDataTimer->setInterval( 600 );
	_pushDataTimer->start();
}

void XTcpSocketWorker::sendData( QByteArray msg )
{
	_outcomeDataBuffer.push_back( msg );
	if ( !_pushDataTimer->isActive() )
	{
		_pushDataTimer->start();
	}
}

QAbstractSocket::SocketState XTcpSocketWorker::connectionState()
{
	return _socket->state();
}

void XTcpSocketWorker::disconnectFromHost()
{
	_timer->stop();
	if ( _socket->state() == QTcpSocket::ConnectedState )
	{
		_socket->disconnectFromHost();
		if ( _socket->state() != QTcpSocket::UnconnectedState )
		{
			_socket->waitForDisconnected();
		}
	}
}

void XTcpSocketWorker::checkConnectionState()
{
	if ( _automaticReconnect )
	{
		if ( _socket != nullptr )
		{
			if ( !( _socket->state() == QTcpSocket::ConnectingState || _socket->state() == QTcpSocket::ConnectedState ) )
			{
				if ( _reconnectionTry > 10 )
				{
					if ( _timer->interval() != _reconnectInterval )
					{
						_timer->setInterval( _reconnectInterval );
					}
				}
				_socket->connectToHost( _host, _port );
				_socket->waitForConnected();
				++_reconnectionTry;
			}
		}
	}
}

void XTcpSocketWorker::readData()
{
	QByteArray data = _socket->readAll();
	if ( !data.isEmpty() )
	{
		for ( char i : data )
		{
			if ( i != _eof )
			{
				_incomeDataBuffer.push_back( i );
			}
			else
			{
				emit dataObtain( _incomeDataBuffer );
				_incomeDataBuffer.clear();
			}
		}
	}
}

void XTcpSocketWorker::pushData()
{
	while ( !_outcomeDataBuffer.isEmpty() )
	{
		auto msg = _outcomeDataBuffer.takeFirst();
		if ( _socket->state() == QTcpSocket::ConnectedState )
		{
			if ( !msg.isEmpty() )
			{
				msg.replace( _eof, ' ' );
				msg.append( _eof );
				_socket->write( msg );
				_socket->waitForBytesWritten();
			}
		}
		else
		{
			_outcomeDataBuffer.push_front( msg );
			emit error( QAbstractSocket::NetworkError );
			break;
		}
	}
	if ( _outcomeDataBuffer.isEmpty() )
	{
		_pushDataTimer->stop();
	}
}

QTcpSocket* XTcpSocketWorker::socket() const
{
	return _socket;
}
