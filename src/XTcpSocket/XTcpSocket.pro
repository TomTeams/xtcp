#-------------------------------------------------
#
# Project created by QtCreator 2019-01-01T21:31:35
#
#-------------------------------------------------
CONFIG += c++11
QT       += network
QT       -= gui
QMAKE_CXXFLAGS += -fopenmp
QMAKE_CXXFLAGS += -static-libstdc++
QMAKE_CFLAGS += -static-libstdc++
QMAKE_LFLAGS  += -static-libstdc++
TARGET = XTcpSocket
TEMPLATE = lib
DESTDIR = $$PWD/../../Deploy/

DEFINES += XTCPSOCKET_LIBRARY

SOURCES += \
        src/XTcpSocket.cpp \
        src/XTcpSocketWorker.cpp

HEADERS += \
        include/XTcpSocket.h \
        include/xtcpsocket_global.h \
        include/XTcpSocketWorker.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
INCLUDEPATH += $$PWD/include
