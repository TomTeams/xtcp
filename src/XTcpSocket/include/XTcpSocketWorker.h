#ifndef XTCPSOCKETWORKER_H
#define XTCPSOCKETWORKER_H
#include <QHostAddress>
#include <QObject>
#include <QQueue>
#include <QTcpSocket>
#include <QTimer>
class XTcpSocketWorker : public QObject
{
	Q_OBJECT
public:
	XTcpSocketWorker( QString host, quint16 port, char eof = char( 04 ), bool automaticReconnect = true, quint16 reconnectInterval = 1000 );
	XTcpSocketWorker( qintptr socketDescriptor, char eof = char( 04 ), bool automaticReconnect = true, quint16 reconnectInterval = 1000 );
	~XTcpSocketWorker() override;

	QTcpSocket* socket() const;

public slots:
	void sendData( QByteArray msg );
	QTcpSocket::SocketState connectionState();


signals:
	void error( QTcpSocket::SocketError error );
	void dataObtain( const QByteArray& data );
	void connectionStateChanged( QAbstractSocket::SocketState state );
	void peerAddressChanged( QHostAddress address );
	void disconnected();

protected slots:
	void checkConnectionState();
	void disconnectFromHost();
	void readData();
	void pushData();

private:
	QString _host;
	quint16 _port;
	QByteArray _incomeDataBuffer;
	QQueue< QByteArray > _outcomeDataBuffer;
	char _eof;
	bool _automaticReconnect;
	uint _reconnectionTry;
	quint16 _reconnectInterval;
	QTimer* _timer;
	QTimer* _pushDataTimer;
	QTcpSocket* _socket;
	void setupTimers();
	void setupConnection();
};

#endif // XTCPSOCKETWORKER_H
