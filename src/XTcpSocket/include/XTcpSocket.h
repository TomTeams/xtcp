#ifndef XTCPSOCKET_H
#define XTCPSOCKET_H
#include "xtcpsocket_global.h"
#include <QHostAddress>
#include <QTcpSocket>
#include <QThread>
#include <atomic>
class XTCPSOCKETSHARED_EXPORT XTcpSocket : public QThread
{
	Q_OBJECT
public:
	XTcpSocket( QString host, quint16 port, char eof = char( 04 ), bool automaticReconnect = true, quint16 reconnectInterval = 1000 );
	XTcpSocket( qintptr socketDescriptor, char eof = char( 04 ), bool automaticReconnect = true, quint16 reconnectInterval = 1000 );
	~XTcpSocket() override;

	//control function
	void sendData( QByteArray msg );
	void disconnectFromHost();
	void setEof( char eof );

	//information function
	QTcpSocket::SocketState connectionState();
	char eof() const;
	QHostAddress peerAddress() const;
	quint16 peerPort() const;

	QTcpSocket* socket() const;
	void setSocket( QTcpSocket* socket );

	bool started() const;

signals:
	void error( QTcpSocket::SocketError error );
	void dataObtain( const QByteArray& data );
	void disconnected();
	void connectionStateChanged( QAbstractSocket::SocketState state );
	void peerAddressChanged( QHostAddress address );

	//internal socket
	void dropHostConnection();
	void pushData( QByteArray data );


	// QThread interface

protected:
	void run() override;

private:
	QString _host;
	quint16 _port;
	char _eof;
	bool _automaticReconnect;
	quint16 _reconnectInterval;
	QAbstractSocket::SocketState _state;
	QTcpSocket* _socket;
	qintptr _socketDescriptor;
	QHostAddress _address;
	bool _started;

private slots:
	void setConnectionState( QAbstractSocket::SocketState state );
	void setPeerAddress( QHostAddress address );
};

#endif // XTCPSOCKET_H
