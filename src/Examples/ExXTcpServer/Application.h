#ifndef APPLICATION_H
#define APPLICATION_H
#include "XTcpClient.h"
#include "XTcpServer.h"
#include <QCoreApplication>
#include <QTimer>
class Application : public QCoreApplication
{
	Q_OBJECT
public:
	explicit Application( int& argc, char** argv );
	enum DISCONNECT_MODE
	{
		SERVER,
		CLIENT
	};
public slots:
	void sendData();
	void switchServerListening();
	void dropStartClients();

private:
	XTcpServer* _server;
	XTcpClient* _client1;
	XTcpClient* _client2;
	QTimer _timer;
	QTimer _disconnectTimer;
	DISCONNECT_MODE _mode;
};

#endif // APPLICATION_H
