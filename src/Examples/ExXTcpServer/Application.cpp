#include "Application.h"
#include "../../XTcpClient/XTcpClient.h"
#include "../../XTcpServer/XTcpServer.h"
#include <QElapsedTimer>
#include <QHostAddress>
#include <QTcpSocket>
#include <QThread>
#include <iostream>
const QString HOST = "172.28.33.35";
const quint16 PORT = 9090;
Application::Application( int& argc, char** argv )
	: QCoreApplication( argc, argv )
	, _server( new XTcpServer( this ) )
	, _client1( new XTcpClient( HOST, PORT ) )
	, _client2( new XTcpClient( HOST, PORT ) )
	, _mode( CLIENT )
{
	_server->listen( QHostAddress( HOST ), PORT );
	connect( _server, &XTcpServer::dataObtain, this, [this]( const ConnectionKey& connectionKey, const QByteArray& data ) {
		qWarning() << "data income to server" << data;
		if ( auto server = static_cast< XTcpServer* >( sender() ) )
		{
			qWarning() << "server try to send data";
			server->send( connectionKey, QByteArray( "respond" ) );
		}
	} );
	connect( _server, &XTcpServer::error, this, []( QAbstractSocket::SocketError error ) { qWarning() << "server has error " << error; } );
	connect( _server, &XTcpServer::acceptError, this, []( QAbstractSocket::SocketError error ) { qWarning() << error; } );

	connect(
		_client1, &XTcpClient::error, this, []( QAbstractSocket::SocketError error ) { qWarning() << "client 1 has error " << error; } );
	connect( _client1, &XTcpClient::dataObtain, this, []( const QByteArray& data ) { qWarning() << "client 1 obtain data" << data; } );

	connect(
		_client2, &XTcpClient::error, this, []( QAbstractSocket::SocketError error ) { qWarning() << "client 2 has error" << error; } );
	connect( _client2, &XTcpClient::dataObtain, this, []( const QByteArray& data ) { qWarning() << "client 2 obtain data" << data; } );
	connect( &_timer, &QTimer::timeout, this, &Application::sendData );
	connect( &_disconnectTimer, &QTimer::timeout, this, &Application::switchServerListening );
	connect( &_disconnectTimer, &QTimer::timeout, this, &Application::dropStartClients );

	_client1->connectToServer();
	_client2->connectToServer();

	_disconnectTimer.setInterval( 4000 );
	_disconnectTimer.start();

	_timer.setInterval( 1000 );
	_timer.start();
}

void Application::sendData()
{

	qWarning() << "*******************";
	qWarning() << "is listen" << _server->isListening();
	if ( _client1 != nullptr )
	{
		qWarning() << "client 1 connection state" << _client1->connectionState();
		_client1->sendData( QByteArray( "data  1" ) );
	}
	if ( _client2 != nullptr )
	{
		qWarning() << "client 1 connection state" << _client2->connectionState();
		_client2->sendData( QByteArray( "data  2" ) );
	}
}

void Application::switchServerListening()
{
	if ( _mode == SERVER )
	{
		qWarning() << "**************************************";
		qWarning() << "switch server";
		if ( _server->isListening() )
		{
			_server->close();
			_server->closeConnections();
			_disconnectTimer.stop();
		}
		else
		{
			_server->listen( QHostAddress( HOST ), PORT );
		}
	}
}

void Application::dropStartClients()
{
	if ( _mode == CLIENT )
	{
		qWarning() << "**************************************";
		qWarning() << "switch clients";
		if ( _client1 != nullptr && _client2 != nullptr )
		{
			delete _client1;
			delete _client2;
			_client1 = nullptr;
			_client2 = nullptr;
		}
		else
		{
			_client1 = new XTcpClient( HOST, PORT );
			_client2 = new XTcpClient( HOST, PORT );
			connect( _client1, &XTcpClient::error, this, []( QAbstractSocket::SocketError error ) {
				qWarning() << "client 1 has error " << error;
			} );
			connect(
				_client1, &XTcpClient::dataObtain, this, []( const QByteArray& data ) { qWarning() << "client 1 obtain data" << data; } );

			connect( _client2, &XTcpClient::error, this, []( QAbstractSocket::SocketError error ) {
				qWarning() << "client 2 has error" << error;
			} );
			connect(
				_client2, &XTcpClient::dataObtain, this, []( const QByteArray& data ) { qWarning() << "client 2 obtain data" << data; } );
			_client1->connectToServer();
			_client2->connectToServer();
		}
	}
}
