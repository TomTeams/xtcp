QT -= gui
QT += network
CONFIG += c++11 console
CONFIG -= app_bundle

SOURCES += \
        main.cpp \
    Application.cpp

DESTDIR = $$PWD/../../../Deploy/


LIBS += -L$$PWD/../../../Deploy/ -lXTcpServer
LIBS += -L$$PWD/../../../Deploy/ -lXTcpClient
LIBS += -L$$PWD/../../../Deploy/ -lXTcpSocket

INCLUDEPATH += $$PWD/../../../Deploy
DEPENDPATH += $$PWD/../../../Deploy

HEADERS += \
    Application.h
